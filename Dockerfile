FROM composer

# ENV ERROR_TRACKING_DSN=https://glet_203exxxxh3@gitlab.com/api/v4/error_tracking/collector/19618667

RUN php --version
COPY composer.json .
RUN composer install

COPY log.php .


CMD [ "php", "./log.php" ]
