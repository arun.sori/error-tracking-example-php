# Error Tracking PHP example

## Testing

```bash
cp .env.sample .env
# Set ERROR_TRACKING_DSN

docker build -t sentry-php .
docker run --rm -it --env-file .env sentry-php

# Verify error showing up on the configured (ERROR_TRACKING_DSN) project
```

## Verify

![screenshot](/assets/screenshot.png)
