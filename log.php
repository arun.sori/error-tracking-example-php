<?
$dsn = $_ENV['ERROR_TRACKING_DSN'];

require_once __DIR__ . '/vendor/autoload.php';

Sentry\init([
    'dsn' => $dsn,
]);

Sentry\captureException(new BadMethodCallException('Message'));
?>
